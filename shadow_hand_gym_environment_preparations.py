
from gazebo_msgs.srv import SetJointStates, SpawnEntity, DeleteModel
import rospy
import random
import numpy as np
from sensor_msgs.msg import Image
from shadow_hand_contact_sensor.msg import shadow_hand_contact_force
from sensor_msgs.msg import JointState
from std_srvs.srv import Empty
shadow_hand_joint_dic={
                        "wrist":["rh_WRJ1","rh_WRJ0"],
                        "index":["rh_FFJ3","rh_FFJ2","rh_FFJ1","rh_FFJ0"],
                        "middle":["rh_MFJ3","rh_MFJ2","rh_MFJ1","rh_MFJ0"],
                        "ring":["rh_RFJ3","rh_RFJ2","rh_RFJ1","rh_RFJ0"],
                        "pinky":["rh_LFJ4","rh_LFJ3","rh_LFJ2","rh_LFJ1","rh_LFJ0"],
                        "thumb":["rh_THJ4","rh_THJ3","rh_THJ2","rh_THJ1","rh_THJ0"]
                      }



"""
****************************************************************************************************
            Proof script for control shadow hand via ROS SERVICES  starts here
    That will prove that the joint 1 and 0 are coupled successfully.
****************************************************************************************************
"""
def apply_action_ros_service(action_list):
    # Action order in Mujoco
    # 'robot0:A_WRJ1', 'robot0:A_WRJ0', 'robot0:A_FFJ3', 'robot0:A_FFJ2', 'robot0:A_FFJ1', 'robot0:A_MFJ3', 'robot0:A_MFJ2', 'robot0:A_MFJ1', 'robot0:A_RFJ3', 'robot0:A_RFJ2', 'robot0:A_RFJ1', 'robot0:A_LFJ4', 'robot0:A_LFJ3', 'robot0:A_LFJ2', 'robot0:A_LFJ1', 'robot0:A_THJ4', 'robot0:A_THJ3', 'robot0:A_THJ2', 'robot0:A_THJ1', 'robot0:A_THJ0'
    tmp = [rospy.ServiceProxy("/shadowhand_motor/" + joint_name + "/set_target", SetJointStates) for finger_name in shadow_hand_joint_dic.keys() for joint_name in shadow_hand_joint_dic[finger_name]]
    for i, act in enumerate(tmp):
        if i in [5,9,13,18]:
            act([action_list[i-1]])
        else:
            act([action_list[i]])
           
# action_list = [random.uniform(0, 1.57) for i in range(24)]
action_list = [0 for i in range(24)]
action_list[4] = 0
action_list[8] = 1.57
action_list[12] = 1.57
action_list[17] = 1.57

# apply_action_ros_service(action_list)

"""
****************************************************************************************************
           Proof script for control shadow hand Via ROS SERVICES  ends here
****************************************************************************************************
"""


"""
****************************************************************************************************
            Proof script for control shadow hand via ROS TOPICS  starts here
    That will prove that the joint 1 and 0 are coupled successfully.
****************************************************************************************************
"""

from std_msgs.msg import Float64
rospy.init_node('shadow_hand_gym')
def apply_action_ros_topics(action_list):
    # Action order in Mujoco
    # 'robot0:A_WRJ1', 'robot0:A_WRJ0', 'robot0:A_FFJ3', 'robot0:A_FFJ2', 'robot0:A_FFJ1', 'robot0:A_MFJ3', 'robot0:A_MFJ2', 'robot0:A_MFJ1', 'robot0:A_RFJ3', 'robot0:A_RFJ2', 'robot0:A_RFJ1', 'robot0:A_LFJ4', 'robot0:A_LFJ3', 'robot0:A_LFJ2', 'robot0:A_LFJ1', 'robot0:A_THJ4', 'robot0:A_THJ3', 'robot0:A_THJ2', 'robot0:A_THJ1', 'robot0:A_THJ0'
    tmp = [rospy.Publisher("/shadowhand_motor/" + joint_name + "/cmd_pos", Float64, queue_size=10) for finger_name in shadow_hand_joint_dic.keys() for joint_name in shadow_hand_joint_dic[finger_name]]
    rate = rospy.Rate(50) # 10hz
    while not rospy.is_shutdown():
        for i, act in enumerate(tmp):
            if i in [5,9,13,18]:
                act.publish(action_list[i-1])
            else:
                act.publish(action_list[i])
                rospy.loginfo(action_list[i])

    
     
        
            
# action_list = [random.uniform(0, 1.57) for i in range(24)]
action_list = [0 for i in range(24)]
action_list[4] = 0
action_list[8] = 1.57
action_list[12] = 1.57
action_list[17] = 1.57

# apply_action_ros_topics(action_list)

"""
****************************************************************************************************
           Proof script for control shadow hand Via ROS TOPICS  ends here
****************************************************************************************************
"""





















"""
****************************************************************************************************
            Proof script for getting camera values starts here
****************************************************************************************************
"""

def camera_callback(data):
    try:
        camera_pixels = np.frombuffer(np.array(data.data),np.uint8).reshape(200,200,3)
    except ValueError:
        print("Your camera size is not correct.")         
    print(camera_pixels.shape)

    rospy.signal_shutdown("Shutdown the Process...")



rospy.Subscriber("/shadow_hand/camera/image_raw", Image, camera_callback)

"""
****************************************************************************************************
            Proof script for getting camera values ends here
****************************************************************************************************
"""

"""
****************************************************************************************************
            Proof script for getting contanct force values starts here
****************************************************************************************************
"""
def contact_force_callback(data):
    print(data.contact_visual_tag_name)
    # Getting touch sensor force in z direction
    touch_sensor_values = np.array([contact_force.z for contact_force in data.force_array])
    print(touch_sensor_values.shape)
    

# rospy.Subscriber("/shadow_hand_visual_tag_contact_sensor", shadow_hand_contact_force, contact_force_callback)


"""
****************************************************************************************************
            Proof script for getting contanct force values ends here
****************************************************************************************************
"""


"""
****************************************************************************************************
            Proof script for getting joint position,velocityies values starts here
****************************************************************************************************
"""

def joint_pos_and_vel_callback(data):
    print(len(data.position[0:-1]))
    joint_pos = np.array(data.position[0:-1])
    joint_vel = np.array(data.velocity[0:-1])

    

# rospy.Subscriber("/shadowhand_motor/joint_states", JointState, joint_pos_and_vel_callback)

"""
****************************************************************************************************
            Proof script for getting joint position,velocityies values ends here
****************************************************************************************************
"""



def reset_simulation():
    rospy.wait_for_service('gazebo/reset_world')
    gazebo_reset_world = rospy.ServiceProxy("/gazebo/reset_world",Empty)
    gazebo_reset_world()
    initial_positions = [-0.16514339750464327, -0.31973286565062153,0.14340512546557435,0.32028208333591573,0.7126053607727917,0.6705281001412586,0.000246444303701037,0.3152655251085491,0.7659800313729842,0.7323156897425923,0.00038520700007378114,0.36743546201985233,0.7119514095008576,0.6699446327514138,0.0525442258033891,-0.13615534724474673,0.39872030433433003,0.7415570009679252,0.704096378652974,0.003673823825070126,0.5506291436028695, -0.014515151997119306,-0.0015229223564485414,-0.7894883021600622]
    # gazebo_reset_sim = rospy.ServiceProxy("/gazebo/reset_sim ",Empty)
    apply_action_ros_service(initial_positions)


# reset_simulation()
# rate = rospy.Rate(100) # 100hz
# while not rospy.is_shutdown():
#     rate.sleep()

# rospy.signal_shutdown("now")
rospy.spin()

